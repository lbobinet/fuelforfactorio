select
	blueprints.title,
	array_agg(distinct entities.entity_name),
	array_agg(distinct entities.recipe_name)
from
	blueprints
	inner join entities on blueprints.id = entities.blueprint_id
where
	blueprints.id in
		(select blueprint_id from entities where entities.recipe_name = 'advanced-circuit')
group by blueprints.title;

select
	blueprints.title,
	array_agg(distinct entities.entity_name),
	array_agg(distinct entities.recipe_name)
from
	blueprints
	inner join entities on blueprints.id = entities.blueprint_id
where
	exists (select 1 from entities where entities.recipe_name = 'advanced-circuit' and entities.blueprint_id = blueprints.id)
and
	exists (select 1 from entities where entities.recipe_name = 'copper-cable' and entities.blueprint_id = blueprints.id)
group by blueprints.title;

select
	blueprints.title,
	array_agg(distinct entities.entity_name),
	array_agg(distinct entities.recipe_name)
from
	blueprints
	inner join entities on blueprints.id = entities.blueprint_id
where
    (select count(distinct entities.recipe_name) from entities where blueprint_id=1 and recipe_name in ('advanced-circuit','copper-cable')) = 2
group by blueprints.title;