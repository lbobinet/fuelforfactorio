#!/bin/bash

curl --cookie-jar /tmp/factocookie http://localhost:8088/auth -d '{"email":"test@test.com","password":"test"}' -H "Content-Type: application/json"
curl --cookie /tmp/factocookie http://localhost:8088/blueprint -H "Content-Type: application/json" -d @bp-sample-post-data.json