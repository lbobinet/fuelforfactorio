-- Your SQL goes here

CREATE TABLE tags (
	blueprint_id SERIAL NOT NULL,
    tag_value VARCHAR NOT NULL,
    PRIMARY KEY (blueprint_id, tag_value),
    FOREIGN KEY (blueprint_id) REFERENCES blueprints (id)
);