
CREATE TABLE comments (
    id SERIAL PRIMARY KEY,
	blueprint_id SERIAL NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    content TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    FOREIGN KEY (blueprint_id) REFERENCES blueprints (id),
    FOREIGN KEY (user_id) REFERENCES users (email)
);
