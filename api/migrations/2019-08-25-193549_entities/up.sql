-- Your SQL goes here

CREATE TABLE entities (
	entity_name VARCHAR NOT NULL,
    recipe_name VARCHAR NOT NULL,
	entity_count INTEGER NOT NULL,
	blueprint_id SERIAL NOT NULL,
	blueprint_index INTEGER NOT NULL,
    PRIMARY KEY (blueprint_id, blueprint_index, entity_name, recipe_name),
    FOREIGN KEY (blueprint_id) REFERENCES blueprints (id)
);
