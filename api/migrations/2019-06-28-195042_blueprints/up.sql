-- Your SQL goes here

CREATE TABLE blueprints (
    id SERIAL PRIMARY KEY,
    user_id VARCHAR(100) NOT NULL,
    title VARCHAR NOT NULL,
    subtitle VARCHAR,
    description TEXT NOT NULL,
    is_book BOOLEAN NOT NULL,
    encoded TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL,
    last_updated_at TIMESTAMP NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (email)
);

CREATE TABLE stars (
	blueprint_id SERIAL NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    created_at TIMESTAMP NOT NULL,
    PRIMARY KEY (blueprint_id, user_id),
    FOREIGN KEY (blueprint_id) REFERENCES blueprints (id),
    FOREIGN KEY (user_id) REFERENCES users (email)
);
