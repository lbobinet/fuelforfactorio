use serde::{Serialize, Deserialize};
use crate::schema::{users,invitations, blueprints, blacklist};
use chrono::{NaiveDateTime, Local};
use uuid::Uuid;
use diesel::r2d2::{ConnectionManager};
use diesel::pg::PgConnection;
use actix::{Actor, SyncContext};
use diesel::sql_types::{Array, BigInt, Text};

pub type Pool = diesel::r2d2::Pool<ConnectionManager<PgConnection>>;

/// This is db executor actor. can be run in parallel
pub struct DbExecutor(pub Pool);


// Actors communicate exclusively by exchanging messages.
// The sending actor can optionally wait for the response.
// Actors are not referenced directly, but by means of addresses.
// Any rust type can be an actor, it only needs to implement the Actor trait.
impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

#[derive(Serialize, Queryable, QueryableByName, Debug)]
#[table_name = "blueprints"]
pub struct BlueprintLight {
    pub id: i32,
    pub title: String,
    pub subtitle: Option<String>,
    #[sql_type = "Text"]
    pub username: String,
    pub is_book: bool,
    pub created_at: NaiveDateTime,
    pub last_updated_at: NaiveDateTime,
    #[sql_type = "Array<Text>"]
    pub tags: Vec<String>,
    #[sql_type = "Array<Text>"]
    pub entities: Vec<String>,
    #[sql_type = "Array<Text>"]
    pub recipes: Vec<String>,
    #[sql_type = "BigInt"]
    pub star_count: i64,
    #[sql_type = "BigInt"]
    pub full_count: i64
}

#[derive(Deserialize, Debug, Clone)]
pub enum SortField {
    CreatedAt,
    StarCount
}

#[derive(Deserialize, Debug, Clone)]
pub struct FilterParams {
    pub page: Option<i64>,
    pub page_size: Option<i64>,
    pub sort_field: Option<SortField>,
    pub recipes: Option<String>,
    pub entities: Option<String>,
    pub tags: Option<String>,
    pub text: Option<String>
}

#[derive(Serialize, Debug)]
pub struct PaginatedBlueprintList {
    pub items: Vec<BlueprintLight>,
    pub nb_pages: i64
}

#[derive(Deserialize, Debug)]
pub struct BlueprintToStore {
    pub title: String,
    pub subtitle: Option<String>,
    pub src_url: Option<String>,
    pub encoded: String,
    pub description: String,
    pub tags: Vec<String>
}

#[derive(Deserialize, Debug)]
pub struct BlueprintToEdit {
    pub id: i32,
    pub title: String,
    pub subtitle: Option<String>,
    pub src_url: Option<String>,
    pub encoded: String,
    pub description: String
}

#[derive(Serialize, Deserialize, Queryable, Identifiable, Debug)]
pub struct Blueprint {
    pub id: i32,
    pub username: String,
    pub title: String,
    pub subtitle: Option<String>,
    pub description: String,
    pub is_book: bool,
	pub encoded: String,
    pub created_at: NaiveDateTime,
    pub last_updated_at: NaiveDateTime,
    pub src_url: Option<String>,
}

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct Star {
    pub blueprint_id: i32,
    pub user_id: String,
    pub created_at: NaiveDateTime
}

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct Comment {
    pub id: i32,
    pub username: String,
    pub content: String,
    pub created_at: NaiveDateTime
}

#[derive(Serialize, Deserialize, Queryable, Debug)]
pub struct Tag {
    pub content: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct CommentToStore {
    pub content: String
}

#[derive(Serialize, Queryable, Debug)]
pub struct EntityInfo {
    pub entity_name: String,
    pub recipe_name: String,
    pub entity_count: i32,
    pub index: i32
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "users"]
pub struct User {
    pub email: String,
    pub username: String,
    pub password: String,
    pub created_at: NaiveDateTime, // only NaiveDateTime works here due to diesel limitations
}

impl User {
    pub fn from_details(email: String, username: String, password: String) -> Self {
        User {
            email,
            username,
            password,
            created_at: Local::now().naive_local(),
        }
    }

    // this is just a helper function to remove password from user just before we return the value out later
    pub fn remove_pwd(mut self) -> Self {
        self.password = "".to_string();
        self
    }
}

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "invitations"]
pub struct Invitation {
    pub id: Uuid,
    pub email: String,
    pub expires_at: NaiveDateTime,
    pub new_user: bool
}

#[derive(Debug, Queryable, Insertable)]
#[table_name = "blacklist"]
pub struct BlacklistItem {
    pub email: String,
    pub registered_at: NaiveDateTime
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SlimUser {
    pub email: String,
    pub username: String,
}

impl From<User> for SlimUser {
    fn from(user: User) -> Self {
        SlimUser { email: user.email, username: user.username }
    }
}
