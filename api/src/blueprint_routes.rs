use actix_web::{web, Error, HttpResponse};
use actix_identity::Identity;
use crate::utils::user_from_identity;

use crate::blueprint_handler::{
    DetailBlueprint,
    ListBlueprint,
    StoreBlueprint,
    EditBlueprint,
    DeleteBlueprint,
    db_list_blueprint,
    db_detail_blueprint,
    db_edit_blueprint,
    db_delete_blueprint,
    db_store_blueprint
};
use crate::models::{BlueprintToStore, BlueprintToEdit, FilterParams, Pool};

pub async fn detail_blueprint(
    id: web::Path<i32>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || db_detail_blueprint(&db.get().unwrap(), DetailBlueprint { blueprint_id: id.into_inner() }))
        .await
        .map(|bp| HttpResponse::Ok().json(bp))?)
}

pub async fn list_blueprint(
    filter_params_query: web::Query<FilterParams>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    let filter_params = filter_params_query.into_inner();
    Ok(web::block(move || db_list_blueprint(&db.get().unwrap(), ListBlueprint {
        filter_params: filter_params.clone(),
        created_by: None,
        starred_by: None,
    }))
        .await
        .map(|bp_list| HttpResponse::Ok().json(bp_list))?)
}

pub async fn my_blueprints(
    filter_params_query: web::Query<FilterParams>,
    db: web::Data<Pool>,
    identity: Identity,
) -> Result<HttpResponse, Error> {
    let filter_params = filter_params_query.into_inner();
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_list_blueprint(&db.get().unwrap(), ListBlueprint {
        filter_params: filter_params.clone(),
        created_by: Some(user.email),
        starred_by: None,
    }))
        .await
        .map(|bp_list| HttpResponse::Ok().json(bp_list))?)
}

pub async fn my_starred_blueprints(
    filter_params_query: web::Query<FilterParams>,
    db: web::Data<Pool>,
    identity: Identity,
) -> Result<HttpResponse, Error> {
    let filter_params = filter_params_query.into_inner();
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_list_blueprint(&db.get().unwrap(), ListBlueprint {
        filter_params: filter_params.clone(),
        created_by: None,
        starred_by: Some(user.email),
    }))
        .await
        .map(|bp_list| HttpResponse::Ok().json(bp_list))?)
}

pub async fn post_blueprint(
    blueprint_data: web::Json<BlueprintToStore>,
    db: web::Data<Pool>,
    identity: Identity) -> Result<HttpResponse, Error> {
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_store_blueprint(&db.get().unwrap(), StoreBlueprint {
        user_id: user.email,
        title: blueprint_data.title.clone(),
        subtitle: blueprint_data.subtitle.clone(),
        src_url: blueprint_data.src_url.clone(),
        description: blueprint_data.description.clone(),
        encoded: blueprint_data.encoded.clone(),
        tags: blueprint_data.tags.clone(),
    }))
        .await
        .map(|_| HttpResponse::Ok().json(()))?)
}

pub async fn edit_blueprint(
    blueprint_data: web::Json<BlueprintToEdit>,
    db: web::Data<Pool>,
    identity: Identity) -> Result<HttpResponse, Error> {
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_edit_blueprint(&db.get().unwrap(), EditBlueprint {
        blueprint_id: blueprint_data.id,
        user_id: user.email,
        title: blueprint_data.title.clone(),
        subtitle: blueprint_data.subtitle.clone(),
        src_url: blueprint_data.src_url.clone(),
        description: blueprint_data.description.clone(),
        encoded: blueprint_data.encoded.clone(),
    }))
        .await
        .map(|bp_list| HttpResponse::Ok().json(bp_list))?)
}

pub async fn delete_blueprint(
    id: web::Path<i32>,
    db: web::Data<Pool>,
    identity: Identity,
) -> Result<HttpResponse, Error> {
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_delete_blueprint(&db.get().unwrap(), DeleteBlueprint {
        blueprint_id: id.into_inner(),
        user_id: user.email,
    }))
        .await
        .map(|bp_list| HttpResponse::Ok().json(bp_list))?)
}