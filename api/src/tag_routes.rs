use actix_web::{web, Error, HttpResponse};
use actix_identity::Identity;
use crate::utils::user_from_identity;

use crate::models::{Pool, Tag};
use crate::tag_handler::*;

pub async fn post_tag(
    blueprint_id: web::Path<i32>,
    tag_value: web::Json<Tag>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(web::block(move || db_insert_tag(&db.get().unwrap(), InsertTag {
        blueprint_id: blueprint_id.into_inner(),
        content: tag_value.into_inner().content,
        user_id: logged_user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn delete_tag(
    blueprint_id: web::Path<i32>,
    tag_value: web::Json<Tag>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(web::block(move || db_delete_tag(&db.get().unwrap(), DeleteTag {
        blueprint_id: blueprint_id.into_inner(),
        content: tag_value.into_inner().content,
        user_id: logged_user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn list_tag(
    blueprint_id: web::Path<i32>,
    db: web::Data<Pool>
    ) -> Result<HttpResponse, Error> {
    Ok(web::block(move || db_list_tag(&db.get().unwrap(), ListTag {
        blueprint_id: blueprint_id.into_inner()
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn list_all_tag(
    db: web::Data<Pool>
    ) -> Result<HttpResponse, Error> {
    Ok(web::block(move || db_list_all_tag(&db.get().unwrap()))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}