use actix_web::{web, Error, HttpResponse};
use serde::Deserialize;

use crate::invitation_handler::*;
use crate::models::Pool;

#[derive(Debug, Deserialize)]
pub struct CreateInvitation {
    pub email: String,
}

pub async fn register_email(
    signup_invitation: web::Json<CreateInvitation>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || create_invitation(&db.get().unwrap(), InvitationRequest {
        email: signup_invitation.email.clone(),
        new_user: true
    }))
        .await
        .map(|_| HttpResponse::Ok().into())?)
}

pub async fn reset_password(
    signup_invitation: web::Json<CreateInvitation>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    Ok(web::block(move || create_invitation(&db.get().unwrap(), InvitationRequest {
        email: signup_invitation.email.clone(),
        new_user: false
    }))
        .await
        .map(|invitation| {dbg!(invitation); HttpResponse::Ok().into()})?)
}