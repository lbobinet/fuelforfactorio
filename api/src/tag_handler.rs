use serde::Deserialize;
use diesel::{self, prelude::*};

use crate::errors::ServiceError;

#[derive(Debug, Deserialize)]
pub struct InsertTag {
    pub blueprint_id: i32,
    pub content: String,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct DeleteTag {
    pub blueprint_id: i32,
    pub content: String,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct ListTag {
    pub blueprint_id: i32
}

pub fn db_insert_tag(conn: &PgConnection, msg: InsertTag) -> Result<(), ServiceError> {

    use crate::schema::tags::dsl::*;
    use crate::schema::blueprints::dsl::*;

    if msg.content.is_empty() {
        return Err(ServiceError::BadRequest("No empty tag".into()));
    }

    let result = blueprints.select(id)
        .filter(id.eq(msg.blueprint_id))
        .filter(user_id.eq(msg.user_id))
        .execute(conn)?;

    if result > 0 {
        let values = (
            blueprint_id.eq(msg.blueprint_id),
            tag_value.eq(msg.content.to_lowercase()));

        diesel::insert_into(tags).values(&values).execute(conn)?;

        Ok(())
    } else {
        Err(ServiceError::BadRequest("Permission denied".into()))
    }
}

pub fn db_delete_tag(conn: &PgConnection, msg: DeleteTag) -> Result<(), ServiceError> {
    use crate::schema::tags::dsl::*;
    use crate::schema::blueprints::dsl::*;

    let result = blueprints.select(id)
        .filter(id.eq(msg.blueprint_id))
        .filter(user_id.eq(msg.user_id))
        .execute(conn)?;

    if result > 0 {
        diesel::delete(
            tags.filter(blueprint_id.eq(&msg.blueprint_id))
                .filter(tag_value.eq(msg.content))).execute(conn)?;
        Ok(())
    } else {
        Err(ServiceError::BadRequest("Permission denied".into()))
    }
}

pub fn db_list_tag(conn: &PgConnection, msg: ListTag) -> Result<Vec<String>, ServiceError> {
    use crate::schema::tags::dsl::*;

    let blueprint_tags = tags
        .select(tag_value)
        .filter(blueprint_id.eq(&msg.blueprint_id))
        .load::<String>(conn)?;

    Ok(blueprint_tags)
}

pub fn db_list_all_tag(conn: &PgConnection) -> Result<Vec<String>, ServiceError> {

    use crate::schema::tags::dsl::*;

    let all_tags = tags
        .select(tag_value)
        .group_by(tag_value)
        .load::<String>(conn)?;

    Ok(all_tags)
}