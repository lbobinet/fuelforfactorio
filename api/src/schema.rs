table! {
    blacklist (email) {
        email -> Text,
        registered_at -> Timestamp,
    }
}

table! {
    blueprints (id) {
        id -> Int4,
        user_id -> Text,
        title -> Text,
        subtitle -> Nullable<Text>,
        description -> Text,
        is_book -> Bool,
        encoded -> Text,
        created_at -> Timestamp,
        last_updated_at -> Timestamp,
        src_url -> Nullable<Text>,
    }
}

table! {
    comments (id) {
        id -> Int4,
        blueprint_id -> Int4,
        user_id -> Text,
        content -> Text,
        created_at -> Timestamp,
    }
}

table! {
    entities (blueprint_id, blueprint_index, entity_name, recipe_name) {
        entity_name -> Text,
        recipe_name -> Text,
        entity_count -> Int4,
        blueprint_id -> Int4,
        blueprint_index -> Int4,
    }
}

table! {
    invitations (id) {
        id -> Uuid,
        email -> Text,
        expires_at -> Timestamp,
        new_user -> Bool,
    }
}

table! {
    stars (blueprint_id, user_id) {
        blueprint_id -> Int4,
        user_id -> Text,
        created_at -> Timestamp,
    }
}

table! {
    tags (blueprint_id, tag_value) {
        blueprint_id -> Int4,
        tag_value -> Text,
    }
}

table! {
    users (email) {
        email -> Text,
        username -> Text,
        password -> Text,
        created_at -> Timestamp,
    }
}

joinable!(blueprints -> users (user_id));
joinable!(comments -> blueprints (blueprint_id));
joinable!(comments -> users (user_id));
joinable!(entities -> blueprints (blueprint_id));
joinable!(stars -> blueprints (blueprint_id));
joinable!(stars -> users (user_id));
joinable!(tags -> blueprints (blueprint_id));

allow_tables_to_appear_in_same_query!(
    blacklist,
    blueprints,
    comments,
    entities,
    invitations,
    stars,
    tags,
    users,
);
