use actix_web::{web, Error, HttpResponse};
use actix_identity::Identity;
use crate::utils::user_from_identity;

use crate::star_handler::{
    InsertStar,
    DeleteStar,
    CheckStar,
    db_delete_star,
    db_get_star,
    db_insert_star
};
use crate::models::Pool;

pub async fn post_star(
    blueprint_id: web::Path<i32>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(web::block(move || db_insert_star(&db.get().unwrap(), InsertStar {
        blueprint_id: blueprint_id.into_inner(),
        user_id: logged_user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn delete_star(
    blueprint_id: web::Path<i32>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_delete_star(&db.get().unwrap(), DeleteStar {
        blueprint_id: blueprint_id.into_inner(),
        user_id: user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn check_star(
    blueprint_id: web::Path<i32>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let user = user_from_identity(identity)?;
    Ok(web::block(move || db_get_star(&db.get().unwrap(), CheckStar {
        blueprint_id: blueprint_id.into_inner(),
        user_id: user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}
