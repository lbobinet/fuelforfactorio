use bcrypt::{hash, DEFAULT_COST};
use crate::errors::ServiceError;
use serde::{Serialize, Deserialize};
use crate::models::SlimUser;
use chrono::{Duration, Local};
use actix_identity::Identity;
use jsonwebtoken::{decode, encode, Header, Validation};

pub type LoggedUser = SlimUser;

pub fn hash_password(plain: &str) -> Result<String, ServiceError> {
    // get the hashing cost from the env variable or use default
    let hashing_cost: u32 = match std::env::var("HASH_ROUNDS") {
        Ok(cost) => cost.parse().unwrap_or(DEFAULT_COST),
        _ => DEFAULT_COST,
    };
    println!("{}", &hashing_cost);
    hash(plain, hashing_cost).map_err(|_| ServiceError::InternalServerError)
}

// JWT claim
#[derive(Debug, Serialize, Deserialize)]
struct Claim {
    // issuer
    iss: String,
    // subject
    sub: String,
    //issued at
    iat: i64,
    // expiry
    exp: i64,
    // user email
    email: String,
    // user name
    username: String
}

impl Claim {
    fn with_values(email: &str, username: &str) -> Self {
        let domain: String =
            std::env::var("DOMAIN").unwrap_or_else(|_| "localhost".to_string());
        Claim {
            iss: domain,
            sub: "auth".into(),
            email: email.to_owned(),
            username: username.to_owned(),
            iat: Local::now().timestamp(),
            exp: (Local::now() + Duration::hours(24)).timestamp(),
        }
    }
}

impl From<Claim> for SlimUser {
    fn from(claims: Claim) -> Self {
        SlimUser {
            email: claims.email,
            username: claims.username,
        }
    }
}

pub fn create_token(data: &SlimUser) -> Result<String, ServiceError> {
    let claims = Claim::with_values(data.email.as_str(), data.username.as_str());
    encode(&Header::default(), &claims, get_secret().as_ref())
        .map_err(|_err| ServiceError::InternalServerError)
}

pub fn user_from_identity(id: Identity) -> Result<LoggedUser, ServiceError> {
    match id.identity() {
        Some(identity) => decode_token(&identity),
        None => Err(ServiceError::Unauthorized.into())
    }
}

pub fn decode_token(token: &str) -> Result<SlimUser, ServiceError> {
    decode::<Claim>(token, get_secret().as_ref(), &Validation::default())
        .map(|data| Ok(data.claims.into()))
        .map_err(|_err| ServiceError::Unauthorized)?
}

fn get_secret() -> String {
    std::env::var("JWT_SECRET").unwrap_or_else(|_| "my secret".into())
}
