use diesel::{self, prelude::*};
use uuid::Uuid;
use crate::models::Invitation;
use crate::errors::ServiceError;
use crate::email_service::send_invitation;
use chrono::{Duration, Local};
use rusoto_core::RusotoError;
use rusoto_ses::{SendEmailError};

#[derive(Debug)]
pub struct InvitationRequest {
    pub email: String,
    pub new_user: bool
}

pub fn create_invitation(conn: &PgConnection, msg: InvitationRequest) -> Result<Invitation, ServiceError> {
    use crate::schema::invitations::dsl::*;
    use crate::schema::blacklist::dsl::*;

    let blacklisted: Vec<String> = blacklist.select(
        crate::schema::blacklist::dsl::email)
        .filter(crate::schema::blacklist::dsl::email.eq(&msg.email))
        .load::<String>(conn)?;
    if !blacklisted.is_empty() {
        return Err(ServiceError::BadRequest("Email blacklisted".into()))
    }

    let existing_invitations: Vec<String> = invitations.select(
        crate::schema::invitations::dsl::email)
        .filter(crate::schema::invitations::dsl::email.eq(&msg.email))
        .filter(crate::schema::invitations::dsl::expires_at.gt(Local::now().naive_local()))
        .load::<String>(conn)?;
    if existing_invitations.len() >= 2 {
        return Err(ServiceError::BadRequest("Already 2 valid invitations".into()))
    }

    let invitation_id = Uuid::new_v4();
    match send_invitation(&invitation_id, &msg.email, msg.new_user) {
        Ok(_) => {
            // creating a new Invitation object with expired at time that is 24 hours from now
            let new_invitation = Invitation {
                id: invitation_id,
                email: msg.email.clone(),
                expires_at: Local::now().naive_local() + Duration::hours(24),
                new_user: msg.new_user
            };

            let inserted_invitation = diesel::insert_into(invitations)
                .values(&new_invitation)
                .get_result(conn)?;

            Ok(inserted_invitation)
        },
        Err(err) => {
            if let RusotoError::Service(send_email_error) = err {
                if let SendEmailError::MessageRejected(rejection_cause) = send_email_error {
                    dbg!(rejection_cause);
                }
            }
            /*let new_item = BlacklistItem {
                email: msg.email.clone(),
                registered_at: Local::now().naive_local() + Duration::hours(24)
            };

            let _inserted_item: BlacklistItem = diesel::insert_into(blacklist)
                .values(&new_item)
                .get_result(conn)?;*/

            Err(ServiceError::BadRequest("Send email refused".into()))
        }
    }
}