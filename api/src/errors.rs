use actix_web::{error::ResponseError, HttpResponse};
use diesel::result::{DatabaseErrorKind, Error as DBError};
use std::convert::From;
use derive_more::Display;
use uuid::parser::ParseError;
use base64::DecodeError;
use rusoto_core::RusotoError;

#[derive(Debug, Display)]
pub enum ServiceError {
    InternalServerError,

    BadRequest(String),

    Unauthorized,
}

// impl ResponseError trait allows to convert our errors into http responses with appropriate data
impl ResponseError for ServiceError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            ServiceError::InternalServerError => HttpResponse::InternalServerError()
                .json("Internal Server Error, Please try later"),
            ServiceError::BadRequest(ref message) => {
                HttpResponse::BadRequest().json(message)
            }
            ServiceError::Unauthorized => {
                HttpResponse::Unauthorized().json("Unauthorized")
            }
        }
    }
}

// we can return early in our handlers if UUID provided by the user is not valid
// and provide a custom message
impl From<ParseError> for ServiceError {
    fn from(_: ParseError) -> ServiceError {
        ServiceError::BadRequest("Invalid UUID".into())
    }
}

impl From<DBError> for ServiceError {
    fn from(error: DBError) -> ServiceError {
        // Right now we just care about UniqueViolation from diesel
        // But this would be helpful to easily map errors as our app grows
        match error {
            DBError::DatabaseError(kind, info) => {
                if let DatabaseErrorKind::UniqueViolation = kind {
                    let message =
                        info.details().unwrap_or_else(|| info.message()).to_string();
                    return ServiceError::BadRequest(message);
                }
                ServiceError::InternalServerError
            }
            _ => ServiceError::InternalServerError,
        }
    }
}

impl From<DecodeError> for ServiceError {
    fn from(_error: DecodeError) -> ServiceError {
        ServiceError::BadRequest("base64 decoding error".into())
    }
}

impl From<std::io::Error> for ServiceError {
    fn from(_error: std::io::Error) -> ServiceError {
        ServiceError::BadRequest("IO error".into())
    }
}

impl<T> From<RusotoError<T>> for ServiceError {
    fn from(_error: RusotoError<T>) -> ServiceError {
        ServiceError::BadRequest("AWS error".into())
    }
}

impl From<serde_json::error::Error> for ServiceError {
    fn from(_error: serde_json::error::Error) -> ServiceError {
        ServiceError::BadRequest("JSON serializer error".into())
    }
}