extern crate openssl;
#[macro_use]
extern crate diesel;

mod models;
mod invitation_routes;
mod blueprint_routes;
mod register_routes;
mod auth_routes;
mod star_routes;
mod comment_routes;
mod tag_routes;
mod schema;

mod invitation_handler;
mod blueprint_handler;
mod register_handler;
mod star_handler;
mod comment_handler;
mod tag_handler;
mod auth_handler;
mod errors;
mod utils;
mod pagination;
mod email_service;

use actix_web::{HttpServer, middleware, web, App};
use actix_identity::{IdentityService, CookieIdentityPolicy};
use actix_web::http;
use actix_cors::Cors;
use chrono::Duration;
use diesel::r2d2::{self, ConnectionManager};
use actix_web::FromRequest;
use flexi_logger::{Logger, Record, DeferredNow};
use diesel::prelude::PgConnection;


pub fn log_format(
    w: &mut dyn std::io::Write,
    now: &mut DeferredNow,
    record: &Record,
) -> Result<(), std::io::Error> {
    write!(
        w,
        "[{}] {} [{}] {}",
        now.now().format("%Y-%m-%d %H:%M:%S"),
        record.level(),
        record.module_path().unwrap_or("<unnamed>"),
        &record.args()
    )
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {

    std::env::set_var("RUST_LOG", "actix_web=info");
    dotenv::dotenv().ok();

    Logger::with_env_or_str("info")
        .format(log_format)
        .log_to_file()
        .directory("logs")
        .start_with_specfile("logs.toml")
        .unwrap_or_else(|e| panic!("Logger initialization failed with {}", e));
    
    let port: String =
        std::env::var("PORT").unwrap_or_else(|_| "8088".to_string());
    let cors_allowed_origin: String =
        std::env::var("CORS_ALLOWED_ORIGIN").unwrap_or_else(|_| "http://localhost:1234".to_string());

    let connspec = std::env::var("DATABASE_URL").expect("DATABASE_URL");
    let manager = ConnectionManager::<PgConnection>::new(connspec);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool.");

	let server = HttpServer::new(move || {

        // secret is a random minimum 32 bytes long base 64 string
        let secret: String =
            std::env::var("SECRET_KEY").unwrap_or_else(|_| "0123".repeat(8));
        let domain: String =
            std::env::var("DOMAIN").unwrap_or_else(|_| "localhost".to_string());

        let logger = middleware::Logger::new(r#"%a "%r" %s %b %T"#);

		App::new()
        .data(pool.clone())
        .wrap(logger)
        .wrap(IdentityService::new(
                CookieIdentityPolicy::new(secret.as_bytes())
                    .name("auth")
                    .path("/")
                    .domain(domain.as_str())
                    .max_age_time(Duration::days(1))
                    .secure(false), // this can only be true if you have https
            ))
        .wrap(
                Cors::new()
                    .allowed_origin(cors_allowed_origin.as_str())
                    .allowed_methods(vec!["GET", "POST", "DELETE", "PUT", "OPTIONS"])
                    .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT, http::header::ORIGIN])
                    .allowed_header(http::header::CONTENT_TYPE)
                    .supports_credentials()
                    .max_age(3600)
                    .finish())
        .service(web::resource("/api/blueprint/mine")
            .route(web::get().to(blueprint_routes::my_blueprints)))
        .service(web::resource("/api/blueprint/starred")
            .route(web::get().to(blueprint_routes::my_starred_blueprints)))
        .service(web::resource("/api/blueprint/{id}")
            .route(web::get().to(blueprint_routes::detail_blueprint))
            .route(web::delete().to(blueprint_routes::delete_blueprint)))
        .service(web::resource("/api/blueprint")
            .app_data(web::Json::<models::BlueprintToStore>::configure(|cfg| {
                cfg.limit(1024 * 1024)
            }))
            .route(web::get().to(blueprint_routes::list_blueprint))
            .route(web::put().to(blueprint_routes::edit_blueprint))
            .route(web::post().to(blueprint_routes::post_blueprint)))
        .service(web::resource("/api/star/{id}")
            .route(web::get().to(star_routes::check_star))
            .route(web::post().to(star_routes::post_star))
            .route(web::delete().to(star_routes::delete_star)))
        .service(web::resource("/api/comment/{id}")
            .route(web::get().to(comment_routes::list_comment))
            .route(web::post().to(comment_routes::post_comment))
            .route(web::delete().to(comment_routes::delete_comment)))
        .service(web::resource("/api/tag/{id}")
            .route(web::get().to(tag_routes::list_tag))
            .route(web::post().to(tag_routes::post_tag)))
        .service(web::resource("/api/tag")
            .route(web::get().to(tag_routes::list_all_tag)))
        .service(web::resource("/api/rmtag/{id}")
            .route(web::post().to(tag_routes::delete_tag)))
        .service(web::resource("/api/auth")
            .route(web::get().to(auth_routes::get_me))
            .route(web::post().to(auth_routes::login))
            .route(web::delete().to(auth_routes::logout)))
        .service(web::resource("/api/register").route(web::post().to(invitation_routes::register_email)))
        .service(web::resource("/api/register/{invitation_id}").route(web::post().to(register_routes::register_user)))
        .service(web::resource("/api/reset").route(web::post().to(invitation_routes::reset_password)))
        .service(web::resource("/api/reset/{invitation_id}").route(web::post().to(register_routes::new_password)))
    });
    server.bind(format!("0.0.0.0:{}", port))?.run().await
}
