use actix_web::{web, Error, HttpResponse};
use serde::Deserialize;

use crate::models::Pool;
use crate::register_handler::{
    RegisterNewUser,
    ResetPasswordRequest,
    db_register,
    db_reset_pwd
};

#[derive(Debug, Deserialize)]
pub struct UserData {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Deserialize)]
pub struct PasswordData {
    pub password: String,
}

pub async fn register_user(
    invitation_id: web::Path<String>,
    user_data: web::Json<UserData>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    let msg = RegisterNewUser {
        // into_inner() returns the inner string value from Path
        invitation_id: invitation_id.into_inner(),
        username: user_data.username.clone(),
        password: user_data.password.clone(),
    };

    Ok(web::block(move || db_register(&db.get().unwrap(), msg))
        .await
        .map(|slim_user| HttpResponse::Ok().json(slim_user))?)
}

pub async fn new_password(
    invitation_id: web::Path<String>,
    user_data: web::Json<PasswordData>,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {
    let msg = ResetPasswordRequest {
        // into_inner() returns the inner string value from Path
        invitation_id: invitation_id.into_inner(),
        password: user_data.password.clone(),
    };

    Ok(web::block(move || db_reset_pwd(&db.get().unwrap(), msg))
        .await
        .map(|_| HttpResponse::Ok().json(()))?)
}
