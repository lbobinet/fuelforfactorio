use crate::errors::ServiceError;
use diesel::prelude::*;
use uuid::Uuid;
use chrono::Local;

use crate::models::{Invitation, SlimUser, User};
use crate::utils::hash_password;

#[derive(Debug)]
pub struct RegisterNewUser {
    pub invitation_id: String,
    pub username: String,
    pub password: String,
}

#[derive(Debug)]
pub struct ResetPasswordRequest {
    pub invitation_id: String,
    pub password: String,
}

pub fn db_register(conn: &PgConnection, msg: RegisterNewUser) -> Result<SlimUser, ServiceError> {
    use crate::schema::invitations::dsl::{id, invitations};
    use crate::schema::users::dsl::{username, email, users};

    // try parsing the string provided by the user as url parameter
    // return early with error that will be converted to ServiceError
    let invitation_id = Uuid::parse_str(&msg.invitation_id)?;

    invitations
        .filter(id.eq(invitation_id))
        .load::<Invitation>(conn)
        .map_err(|_db_error| ServiceError::BadRequest("Database access error".into()))
        .and_then(|mut result| {
            if let Some(invitation) = result.pop() {

                let existing_username: Vec<String> = users.select(email)
                    .filter(username.eq(&msg.username))
                    .load::<String>(conn)?;

                if existing_username.is_empty() {
                    diesel::delete(invitations.filter(id.eq(invitation.id)))
                        .execute(conn)?;

                    // if invitation is not expired
                    if invitation.expires_at > Local::now().naive_local() && invitation.new_user {

                        // try hashing the password, else return the error that will be converted to ServiceError
                        let password: String = hash_password(&msg.password)?;
                        let user = User::from_details(invitation.email, msg.username, password);
                        let inserted_user: User =
                            diesel::insert_into(users).values(&user).get_result(conn)?;
                        return Ok(inserted_user.into());
                    } else {
                        return Err(ServiceError::BadRequest("Expired invitation".into()))
                    }
                } else {
                    return Err(ServiceError::BadRequest("Username already used".into()))
                }

            }
            Err(ServiceError::BadRequest("Invalid Invitation".into()))
        })
}

pub fn db_reset_pwd(conn: &PgConnection, msg: ResetPasswordRequest) -> Result<(), ServiceError> {
    use crate::schema::invitations::dsl::{id, invitations};
    use crate::schema::users::dsl::{users, email, password};

    // try parsing the string provided by the user as url parameter
    // return early with error that will be converted to ServiceError
    let invitation_id = Uuid::parse_str(&msg.invitation_id)?;

    invitations
        .filter(id.eq(invitation_id))
        .load::<Invitation>(conn)
        .map_err(|_db_error| ServiceError::BadRequest("Invalid Invitation".into()))
        .and_then(|mut result| {
            if let Some(invitation) = result.pop() {
                diesel::delete(invitations.filter(id.eq(invitation.id)))
                    .execute(conn)?;
                // if invitation is not expired
                if invitation.expires_at > Local::now().naive_local() && !invitation.new_user {
                    // try hashing the password, else return the error that will be converted to ServiceError
                    let new_password: String = hash_password(&msg.password)?;

                    let new_values = password.eq(new_password);

                    diesel::update(users.filter(email.eq(invitation.email)))
                        .set(new_values)
                        .execute(conn)?;

                    return Ok(());
                }
            }
            Err(ServiceError::BadRequest("Invalid Invitation".into()))
        })
}
