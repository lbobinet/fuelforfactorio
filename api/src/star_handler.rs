use diesel::{self, prelude::*};
use serde::Deserialize;
use chrono::Local;
use crate::errors::ServiceError;

#[derive(Debug, Deserialize)]
pub struct InsertStar {
    pub blueprint_id: i32,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct DeleteStar {
    pub blueprint_id: i32,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct CheckStar {
    pub blueprint_id: i32,
    pub user_id: String
}

pub fn db_insert_star(conn: &PgConnection, msg: InsertStar) -> Result<bool, ServiceError> {
    use crate::schema::stars::dsl::*;

    let values = (
        blueprint_id.eq(msg.blueprint_id),
        user_id.eq(msg.user_id),
        created_at.eq(Local::now().naive_local()));

    diesel::insert_into(stars).values(&values).execute(conn)?;

    Ok(true)
}

pub fn db_delete_star(conn: &PgConnection, msg: DeleteStar) -> Result<bool, ServiceError> {
    use crate::schema::stars::dsl::*;

    diesel::delete(
        stars.filter(blueprint_id.eq(&msg.blueprint_id))
            .filter(user_id.eq(msg.user_id))).execute(conn)?;

    Ok(false)
}

pub fn db_get_star(conn: &PgConnection, msg: CheckStar) -> Result<bool, ServiceError> {
    use crate::schema::stars::dsl::*;

    let star_in_db: Vec<i32> = stars.select(
        crate::schema::stars::dsl::blueprint_id)
        .filter(blueprint_id.eq(&msg.blueprint_id))
        .filter(user_id.eq(msg.user_id))
        .load(conn)?;

    Ok(!star_in_db.is_empty())
}