use bcrypt::verify;

use serde::Deserialize;
use crate::errors::ServiceError;
use crate::models::{SlimUser, User};

use diesel::prelude::*;



#[derive(Debug, Deserialize)]
pub struct AuthData {
    pub email: String,
    pub password: String,
}

pub fn db_login(conn: &PgConnection, msg: AuthData) -> Result<SlimUser, ServiceError> {
    use crate::schema::users::dsl::{email, users};

    let mut items = users.filter(email.eq(&msg.email)).load::<User>(conn)?;

    if let Some(user) = items.pop() {
        if let Ok(matching) = verify(&msg.password, &user.password) {
            if matching {
                return Ok(user.into());
            }
        }
    }
    Err(ServiceError::BadRequest(
        "Username and Password don't match".into(),
    ))
}
