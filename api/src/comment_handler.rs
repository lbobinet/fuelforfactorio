use serde::Deserialize;
use diesel::{self, prelude::*};
use chrono::Local;

use crate::models::Comment;
use crate::errors::ServiceError;

#[derive(Debug, Deserialize)]
pub struct InsertComment {
    pub blueprint_id: i32,
    pub user_id: String,
    pub content: String
}

#[derive(Debug, Deserialize)]
pub struct DeleteComment {
    pub id: i32,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct ListComment {
    pub blueprint_id: i32
}

pub fn db_insert_comment(conn: &PgConnection, msg: InsertComment) -> Result<(), ServiceError> {

    use crate::schema::comments::dsl::*;

    if msg.content.is_empty() {
        return Err(ServiceError::BadRequest("No empty comment".into()));
    }

    let values = (
        blueprint_id.eq(msg.blueprint_id),
        user_id.eq(msg.user_id),
        content.eq(msg.content),
        created_at.eq(Local::now().naive_local()));

    diesel::insert_into(comments).values(&values).execute(conn)?;

    Ok(())
}

pub fn db_delete_comment(conn: &PgConnection, msg: DeleteComment) -> Result<(), ServiceError> {

    use crate::schema::comments::dsl::*;

    diesel::delete(
        comments.filter(id.eq(&msg.id))
            .filter(user_id.eq(msg.user_id))).execute(conn)?;

    Ok(())
}

pub fn db_list_comment(conn: &PgConnection, msg: ListComment) -> Result<Vec<Comment>, ServiceError> {

    use crate::schema::comments::dsl::*;
    use crate::schema::users::dsl::*;

    let blueprint_comments = comments
        .inner_join(users)
        .select((id, username, content, crate::schema::comments::dsl::created_at))
        .filter(blueprint_id.eq(&msg.blueprint_id))
        .order(crate::schema::comments::dsl::created_at.asc())
        .load::<Comment>(conn)?;

    Ok(blueprint_comments)
}
