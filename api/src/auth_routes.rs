use actix_identity::Identity;
use crate::utils::user_from_identity;
use actix_web::{web, Error, HttpResponse, Responder, ResponseError};

use crate::auth_handler::{AuthData, db_login};
use crate::models::Pool;
use crate::utils::create_token;

pub async fn login(
    auth_data: web::Json<AuthData>,
    id: Identity,
    db: web::Data<Pool>,
) -> Result<HttpResponse, Error> {

    Ok(web::block(move || db_login(&db.get().unwrap(), auth_data.into_inner()))
        .await
        .map(|user| {
            match create_token(&user) {
                Ok(token) => {
                    id.remember(token);
                    HttpResponse::Ok().json(user)
                },
                Err(err) => err.error_response()
            }
        })?)
}

pub async fn logout(id: Identity) -> impl Responder {
    id.forget();
    HttpResponse::Ok()
}

pub async fn get_me(identity: Identity) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(HttpResponse::Ok().json(logged_user))
}
