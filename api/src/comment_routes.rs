use actix_web::{web, Error, HttpResponse};
use actix_identity::Identity;
use crate::utils::user_from_identity;

use crate::comment_handler::*;
use crate::models::{Pool, CommentToStore};

pub async fn post_comment(
    blueprint_id: web::Path<i32>,
    comment_data: web::Json<CommentToStore>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(web::block(move || db_insert_comment(&db.get().unwrap(), InsertComment {
        blueprint_id: blueprint_id.into_inner(),
        user_id: logged_user.email,
        content: comment_data.into_inner().content
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn delete_comment(
    comment_id: web::Path<i32>,
    db: web::Data<Pool>,
    identity: Identity
    ) -> Result<HttpResponse, Error> {
    let logged_user = user_from_identity(identity)?;
    Ok(web::block(move || db_delete_comment(&db.get().unwrap(), DeleteComment {
        id: comment_id.into_inner(),
        user_id: logged_user.email
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}

pub async fn list_comment(
    blueprint_id: web::Path<i32>,
    db: web::Data<Pool>
    ) -> Result<HttpResponse, Error> {
    Ok(web::block(move || db_list_comment(&db.get().unwrap(), ListComment {
        blueprint_id: blueprint_id.into_inner()
    }))
        .await
        .map(|res| HttpResponse::Ok().json(res))?)
}