use rusoto_core::{
    Region,
    RusotoError
};
use rusoto_ses::{
    SesClient,
    Ses,
    SendEmailRequest,
    Destination,
    Message,
    Body,
    Content,
    SendEmailResponse,
    SendEmailError
};
use uuid::Uuid;

pub fn send_invitation(invitation_id: &Uuid, email: &String, new_user: bool) -> Result<SendEmailResponse, RusotoError<SendEmailError>> {
    let client = SesClient::new(Region::EuWest1);
    let from_email: String =
        std::env::var("FROM_EMAIL").map_err(|_err|
            RusotoError::Validation("No source email specified".into()))?;

    let domain: String =
        std::env::var("DOMAIN").unwrap_or_else(|_| "localhost".to_string());
    let (subject, link_url) = if new_user {
        (
            "FuelForFactorio registration".to_string(),
            format!("https://{}/register/{}",
                               domain, invitation_id.to_string()))
    } else {
        (
            "FuelForFactorio reset password".to_string(),
            format!("https://{}/reset/{}",
                               domain, invitation_id.to_string()))
    };
    let body_template = if new_user {
        format!(include_str!("template/register_email.html"),
                                  link_url)
    } else {
        format!(include_str!("template/reset_email.html"),
                                  link_url)
    };

    let request = SendEmailRequest {
        source: from_email,
        destination: Destination {
            to_addresses: Some(vec!(email.clone())),
            ..Default::default()
        },
        message: Message {
            subject: Content {
                data: subject,
                ..Default::default()
            },
            body: Body {
                html: Some(Content {
                    data: body_template,
                    ..Default::default()
                }),
                ..Default::default()
            }
        },
        ..Default::default()
    };
    client.send_email(request).sync()
}
