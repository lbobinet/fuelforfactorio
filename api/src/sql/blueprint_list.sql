select
    b.id,
    b.title,
    b.subtitle,
    u.username,
    b.is_book,
    b.created_at,
    b.last_updated_at,
    array(select tag_value from tags t where t.blueprint_id = b.id) as tags,
    array(select e.entity_name
        from entities e
        where e.blueprint_id = b.id
        group by e.entity_name
        order by max(e.entity_count) desc
    ) as entities,
    array(select e.recipe_name
        from entities e
        where e.blueprint_id = b.id and e.recipe_name <> ''
        group by e.recipe_name
        order by max(e.entity_count) desc
    ) as recipes,
    COUNT(distinct s.user_id) as star_count,
    COUNT(*) OVER () AS full_count
from
    blueprints b
    inner join users u on u.email = b.user_id
    left outer join stars s on s.blueprint_id = b.id
{filters}
group by
    b.id,
    u.username
ORDER BY {order_by}
LIMIT {page_size} OFFSET {query_offset}
;