use diesel::{self, prelude::*};
use serde::Deserialize;
use crate::models::{
    Blueprint,
    BlueprintLight,
    EntityInfo,
    PaginatedBlueprintList,
    FilterParams,
    SortField};
use crate::errors::ServiceError;
use base64::decode;
use libflate::zlib::{Decoder};
use std::io::Read;
use chrono::{Local, NaiveDateTime};

#[derive(Debug, Deserialize)]
pub struct ListBlueprint {
    pub filter_params: FilterParams,
    pub created_by: Option<String>,
    pub starred_by: Option<String>,
}

#[derive(Debug, Deserialize)]
pub struct DetailBlueprint {
    pub blueprint_id: i32
}

#[derive(Debug, Deserialize)]
pub struct DeleteBlueprint {
    pub blueprint_id: i32,
    pub user_id: String
}

#[derive(Debug, Deserialize)]
pub struct StoreBlueprint {
    pub title: String,
    pub subtitle: Option<String>,
    pub src_url: Option<String>,
    pub encoded: String,
    pub description: String,
    pub user_id: String,
    pub tags: Vec<String>
}

#[derive(Debug, Deserialize)]
pub struct EditBlueprint {
    pub blueprint_id: i32,
    pub title: String,
    pub subtitle: Option<String>,
    pub src_url: Option<String>,
    pub encoded: String,
    pub description: String,
    pub user_id: String
}

pub fn db_list_blueprint(conn: &PgConnection, msg: ListBlueprint) -> Result<PaginatedBlueprintList, ServiceError> {
    let (items, nb_pages) = list_v2(conn, msg)?;

    Ok(PaginatedBlueprintList {
        items,
        nb_pages
    })
}

fn list_v2(conn: &PgConnection, msg: ListBlueprint) -> Result<(Vec<BlueprintLight>, i64), diesel::result::Error> {
    let page_size = msg.filter_params.page_size.unwrap_or(10);
    let page_index = msg.filter_params.page.unwrap_or(1);
    let offset = (page_index - 1) * page_size;

    let mut filter_list = Vec::<String>::new();

    if let Some(filter_text) = msg.filter_params.text {
        filter_list.push(format!("b.title ilike '%{}%' or b.title ilike '%{}%'", filter_text, filter_text));
    }

    if let Some(email) = msg.created_by {
        filter_list.push(format!("u.email = '{}'", email));
    }

    if let Some(email) = msg.starred_by {
        filter_list.push(format!("s.user_id = '{}'", email));
    }

    if let Some(tags) = msg.filter_params.tags {
        let tag_list: Vec<String> = tags.split(',').map(
            |tag_content|format!("'{}'", tag_content)).collect();
        filter_list.push(format!("(SELECT COUNT(DISTINCT tag_value) FROM tags \
            WHERE tags.blueprint_id = b.id \
            and tags.tag_value in ({})) = {}", tag_list.join(","), tag_list.len()));
    }

    if let Some(recipe_filter) = msg.filter_params.recipes {
        let vec_recipes: Vec<String> = recipe_filter.split(',').map(
            |recipe_name|format!("'{}'", recipe_name)).collect();
        filter_list.push(format!("(SELECT COUNT(DISTINCT recipe_name) FROM entities \
            WHERE entities.blueprint_id = b.id \
            and entities.recipe_name in ({})) = {}", vec_recipes.join(","), vec_recipes.len()));
    }

    if let Some(entity_filter) = msg.filter_params.entities {
        let vec_entities: Vec<String> = entity_filter.split(',').map(
            |entity_name|format!("'{}'", entity_name)).collect();
        filter_list.push(format!("(SELECT COUNT(DISTINCT entity_name) FROM entities \
            WHERE entities.blueprint_id = b.id \
            and entities.entity_name in ({})) = {}", vec_entities.join(","), vec_entities.len()));
    }

    let filters = if filter_list.is_empty() {String::new()} else {format!("WHERE {}", filter_list.join(" and "))};

    let order = if let Some(SortField::StarCount) = msg.filter_params.sort_field {
        "star_count DESC, b.created_at DESC"
    } else {
        "b.created_at DESC"
    };

    let query = format!(include_str!("sql/blueprint_list.sql"), 
        filters = filters,
        page_size = page_size,
        query_offset = offset,
        order_by = order);
    let query_result: Result<Vec<BlueprintLight>, diesel::result::Error> = diesel::dsl::sql_query(query).load(conn);
    match query_result {
        Ok(items) =>  {
            let total = if items.is_empty() {0} else {items.first().unwrap().full_count};
            let total_pages = (total as f64 / page_size as f64).ceil() as i64;
            Ok((items, total_pages))
        },
        Err(e) => {
            Err(e)
        }
    }
}

pub fn db_detail_blueprint(conn: &PgConnection, msg: DetailBlueprint) -> Result<Blueprint, ServiceError> {
    use crate::schema::blueprints;
    use crate::schema::users;
    let mut items = blueprints::dsl::blueprints
        .inner_join(users::dsl::users)
        .filter(blueprints::dsl::id.eq(msg.blueprint_id))
        .select((blueprints::dsl::id,
                 users::dsl::username,
                 blueprints::dsl::title,
                 blueprints::dsl::subtitle,
                 blueprints::dsl::description,
                 blueprints::dsl::is_book,
                 blueprints::dsl::encoded,
                 blueprints::dsl::created_at,
                 blueprints::dsl::last_updated_at,
                 blueprints::dsl::src_url))
        .load::<Blueprint>(conn)?;

    items.pop().ok_or_else(|| ServiceError::BadRequest("Not found".into()))
}

pub fn db_store_blueprint(conn: &PgConnection, msg: StoreBlueprint) -> Result<(), ServiceError> {
    use crate::schema::blueprints::dsl::*;

    let json_value = decode_blueprint(&msg.encoded)?;
    let (book, entity_infos) = extract_infos(json_value)?;

    let new_blueprint = (
        title.eq(msg.title),
        subtitle.eq(msg.subtitle),
        encoded.eq(msg.encoded),
        description.eq(msg.description),
        user_id.eq(msg.user_id),
        is_book.eq(book),
        created_at.eq(Local::now().naive_local()),
        last_updated_at.eq(Local::now().naive_local()),
        src_url.eq(msg.src_url));

    let (blueprint_id, _, _, _, _, _, _, _, _, _):
        (i32, String, String, Option<String>, String, bool,
         String, NaiveDateTime, NaiveDateTime, Option<String>)
        = diesel::insert_into(blueprints)
        .values(&new_blueprint)
        .get_result(conn)?;

    insert_entities_and_recipes(entity_infos, blueprint_id, conn)?;

    insert_tags(blueprint_id, &msg.tags, &conn)?;

    Ok(())
}

pub fn db_edit_blueprint(conn: &PgConnection, msg: EditBlueprint) -> Result<(), ServiceError> {
    use crate::schema::blueprints::dsl::*;

    let json_value = decode_blueprint(&msg.encoded)?;
    let (book, entity_infos) = extract_infos(json_value)?;

    let new_values = (
        title.eq(msg.title.clone()),
        subtitle.eq(msg.subtitle.clone()),
        encoded.eq(msg.encoded.clone()),
        is_book.eq(book),
        description.eq(msg.description.clone()),
        last_updated_at.eq(Local::now().naive_local()),
        src_url.eq(msg.src_url));

    diesel::delete(crate::schema::entities::dsl::entities.filter(
        crate::schema::entities::blueprint_id.eq(&msg.blueprint_id))).execute(conn)?;

    diesel::update(blueprints.filter(id.eq(&msg.blueprint_id))
        .filter(user_id.eq(&msg.user_id))).set(new_values)
        .execute(conn)?;

    insert_entities_and_recipes(entity_infos, msg.blueprint_id, conn)?;

    Ok(())
}

pub fn db_delete_blueprint(conn: &PgConnection, msg: DeleteBlueprint) -> Result<(), ServiceError> {
    use crate::schema::blueprints::dsl::*;
    use crate::schema::tags::dsl::*;

    diesel::delete(crate::schema::entities::dsl::entities.filter(
        crate::schema::entities::blueprint_id.eq(&msg.blueprint_id))).execute(conn)?;

    diesel::delete(crate::schema::stars::dsl::stars.filter(
        crate::schema::stars::blueprint_id.eq(&msg.blueprint_id))).execute(conn)?;

    diesel::delete(crate::schema::comments::dsl::comments.filter(
        crate::schema::comments::blueprint_id.eq(&msg.blueprint_id))).execute(conn)?;

    diesel::delete(tags.filter(blueprint_id.eq(&msg.blueprint_id)))
        .execute(conn)?;

    diesel::delete(blueprints.filter(id.eq(&msg.blueprint_id))
        .filter(user_id.eq(&msg.user_id))).execute(conn)?;

    Ok(())
}

pub fn insert_tags(bp_id: i32, tag_values: &Vec<String>,
        conn: &PgConnection) -> Result<(), ServiceError> {
    let mut tag_iter = tag_values.iter();
    let mut tag_value = tag_iter.next();

    while tag_value.is_some() {
        insert_tag(bp_id, tag_value.unwrap(), conn)?;
        tag_value = tag_iter.next();
    }

    Ok(())
}

pub fn insert_tag(bp_id: i32, value: &String,
        conn: &PgConnection) -> Result<(), ServiceError> {
    use crate::schema::tags::dsl::*;

    diesel::insert_into(tags).values((
        blueprint_id.eq(&bp_id),
        tag_value.eq(&value))).execute(conn)?;

    Ok(())
}

pub fn insert_entities_and_recipes(entity_infos: Vec<EntityInfo>, id: i32,
                               conn: &PgConnection) -> Result<(), ServiceError> {
    let entity_infos_tuple: Vec<_> = entity_infos.iter().map(|elem|(
        crate::schema::entities::dsl::entity_name.eq(&elem.entity_name),
        crate::schema::entities::dsl::recipe_name.eq(&elem.recipe_name),
        crate::schema::entities::dsl::entity_count.eq(&elem.entity_count),
        crate::schema::entities::dsl::blueprint_index.eq(&elem.index),
        crate::schema::entities::dsl::blueprint_id.eq(id))).collect();
    diesel::insert_into(crate::schema::entities::dsl::entities)
        .values(entity_infos_tuple)
        .execute(conn)?;

    Ok(())
}

pub fn decode_blueprint(encoded_value: &str) -> Result<serde_json::Value, ServiceError> {
    let base64_value = encoded_value.get(1..).unwrap();
    let zlib_value: Vec<u8> = decode(base64_value)?;
    let mut decoder = Decoder::new(&zlib_value[..])?;
    let mut json_result = String::new();
    decoder.read_to_string(&mut json_result)?;
    serde_json::from_str(&json_result).map_err(|_|ServiceError::BadRequest("unable to decode value".into()))
}

fn extract_infos_from_single_blueprint<'a>(
    blueprint_json: &serde_json::Value,
    index: i32,
    entities: &'a mut Vec<EntityInfo>)
    -> Result<(), ServiceError> {
    let blueprint = blueprint_json.get("blueprint")
        .ok_or_else(|| ServiceError::BadRequest("invalid blueprint".into()))?;
    let entities_json = blueprint.get("entities")
        .ok_or_else(|| ServiceError::BadRequest("invalid blueprint".into()))?;
    let entity_vec = entities_json.as_array()
        .ok_or_else(|| ServiceError::BadRequest("invalid blueprint".into()))?;

    entity_vec.iter().for_each(|entity| {
        let mut entity_name = String::from(entity.get("name").unwrap().as_str().unwrap());
        if entity_name.ends_with("rail") {
            entity_name = "rail".into();
        }
        let recipe_name = match entity.get("recipe") {
            Some(value) => String::from(value.as_str().unwrap()),
            None => "".into()
        };
        let entity_info = entities.iter_mut().find(|elem|
            elem.entity_name == entity_name && elem.recipe_name == recipe_name && elem.index == index);
        match entity_info {
            Some(info) => info.entity_count += 1,
            None => entities.push(EntityInfo{entity_name, recipe_name, entity_count: 1, index})
        };
    });

    Ok(())
}

pub fn extract_infos(blueprint_json: serde_json::Value)
    -> Result<(bool, Vec<EntityInfo>), ServiceError> {
    let mut entities = Vec::new();
    let book = blueprint_json.get("blueprint_book");
    match book {
        None => {
            extract_infos_from_single_blueprint(&blueprint_json, 0, &mut entities)
        },
        Some(book_root) => {
            let blueprints_root = book_root.get("blueprints")
                .ok_or_else(|| ServiceError::BadRequest("invalid blueprint".into()))?;
            let blueprints_array = blueprints_root.as_array()
                .ok_or_else(|| ServiceError::BadRequest("invalid blueprint".into()))?;
            blueprints_array.iter().enumerate().try_for_each(|(index, bp)| {
                extract_infos_from_single_blueprint(bp, index as i32,
                                                        &mut entities)
            })
        }
    }?;
    Ok((book.is_some(), entities))
}
