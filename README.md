# Fuel for Factorio website source code

## Project setup

### Install rust compilation stable toolchain
See https://rustup.rs/

### Prepare database for backend
```
# go into api directory
$ cd api
# configure the location of your database
$ echo DATABASE_URL=postgres://username:password@localhost/diesel_demo > .env
# install diesel command-line tool
$ cargo install diesel_cli --no-default-features --features postgres
# run sql scripts to initialize database
$ diesel migration run
```
### Compile and run the backend
```
$ cargo run --bin main
```
It will be binded on port 8088 by default

### Run the frontend
```
$ cd ../front
$ yarn install
$ yarn serve
```
No default user is created in the database. To register a user without AWS email service credentials, you can retrieve the invitation UUID with the backend logs, the "invitations" table content in postgresql, or by step-by-step debugging.
