import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userdata: {},
    tagList: [],
    errors: []
  },
  mutations: {
  	userdata(state, userdata) {
    	state.userdata = userdata;
	  },
  	tagList(state, tagList) {
    	state.tagList = tagList;
	  },
  	error(state, error) {
      if(state.errors.length > 1) {
        state.errors.shift();
      }
    	state.errors.push(error);
	  },
  },
  actions: {
    login  ({ commit }, { email, password }) {
      return new Promise((resolve, reject) => {
        axios.post(`/auth`, {
            email: email.trim(),
            password: password.trim()
          }, {withCredentials: true})
        .then((response) => {
          commit('userdata', response.data);
          resolve(response)
        })
        .catch((e) => {
          commit('error', e.response.data)
          reject(e);
        })
      });
    },
    getAuthenticatedUser ({ commit }) {
      axios.get(`/auth`, {withCredentials: true})
      .then(response => {
          commit('userdata', response.data)
      })
    },
    logout ({ commit }) {
      axios.delete(`/auth`, {withCredentials: true})
        .then(response => {
          commit('userdata', {})
        })
    },
    sendRegistrationRequest ({ commit }, email ) {
      return new Promise((resolve, reject) => {
        axios.post('/register', { email: email})
        .then((response) => {
          resolve(response)
        })
        .catch((e) => {
          commit('error', e.response.data)
        })
      });
    },
    sendResetPasswordRequest ({ commit }, email ) {
      return new Promise((resolve, reject) => {
        axios.post('/reset', { email: email})
        .then((response) => {
          resolve(response)
        })
        .catch((e) => {
          commit('error', e.response.data)
          reject(e);
        })
      });
    },
    sendRegistrationData ({ commit }, { invitationId, username, password }) {
      return new Promise((resolve, reject) => {
        axios.post('/register/'+ invitationId, {
          username: username,
          password: password
        })
        .then((response) => {
          resolve(response)
        })
        .catch((e) => {
          commit('error', e.response.data)
          reject(e);
        })
      });
    },
    sendNewPasswordData ({ commit }, { invitationId, password }) {
      return new Promise((resolve, reject) => {
        axios.post('/reset/'+ invitationId, {
          password: password
        })
        .then((response) => {
          resolve(response)
        })
        .catch((e) => {
          commit('error', e.response.data)
          reject(e);
        })
      });
    },
    addTag ({ commit }, { blueprintId, tag }) {
      return new Promise((resolve, reject) => {
        axios.post(`/tag/` + blueprintId, {
            content: tag
          }, {withCredentials: true})
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    removeTag ({ commit }, { blueprintId, tag }) {
      return new Promise((resolve, reject) => {
        axios.post(`/rmtag/` + blueprintId, {
            content: tag
          }, {withCredentials: true})
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    getTags ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.get(`/tag/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    loadExistingTagList ({ commit }) {
      axios.get(`/tag`)
        .then(response => {
          commit('tagList', response.data);
        })
        .catch(e => {
          commit('error', e.response.data)
        })
    },
    getComments ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.get(`/comment/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    removeComment ({ commit }, commentId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/comment/` + commentId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    postComment ({ commit }, { blueprintId, comment }) {
      return new Promise((resolve, reject) => {
        axios.post(`/comment/` + blueprintId, {
          content: comment
        }, {withCredentials: true})
        .then(response => {
          resolve(response)
        })
        .catch(e => {
          commit('error', e.response.data)
          reject(e);
        })
      });
    },
    isFavorite ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.get(`/star/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            reject(e);
          })
      });
    },
    saveAsFavorite ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.post(`/star/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    removeFavorite ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/star/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    },
    deleteBlueprint ({ commit }, blueprintId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/blueprint/` + blueprintId)
          .then(response => {
            resolve(response)
          })
          .catch(e => {
            commit('error', e.response.data)
            reject(e);
          })
      });
    }
  }
})
