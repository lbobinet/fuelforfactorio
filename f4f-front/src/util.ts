import pako from 'pako'

function bin2String(array) {
  var result = "";
  for (var i = 0; i < array.length; i++) {
    result += String.fromCharCode(parseInt(array[i]));
  }
  return result;
}

function decodeBlueprint(encodedBlueprint) {
    let compressed = atob(encodedBlueprint.substring(1))
    let inflated = pako.inflate(compressed);
    return JSON.parse(bin2String(inflated));
}


function blueprintListFromJsonData(blueprintData) {
    if(!blueprintData) {
      return [];
    } else if(blueprintData.blueprint_book) {
      return blueprintData.blueprint_book.blueprints;
    } else {
      return [blueprintData];
    }
}

function convertNameToGameData(entityType) {
  switch(entityType) {
    case 'science-pack-1':
      return 'automation_science_pack'
    case 'science-pack-2':
      return 'logistic_science_pack'
    case 'empty-lubricant-barrel':
      return 'empty_barrel'
    case 'empty-sulfuric-acid-barrel':
      return 'empty_barrel'
    default:
      return entityType.replace(/-/g, '_');
  }
}

function convertGameDataToApiName(entityType) {
  switch(entityType) {
    default:
      return entityType.replace(/_/g, '-');
  }
}

export default {
    decodeBlueprint,
    blueprintListFromJsonData,
    convertNameToGameData,
    convertGameDataToApiName
}