import Vue from 'vue'
import Router from 'vue-router'
import BlueprintView from './views/BlueprintView.vue'
import BlueprintList from './views/BlueprintList.vue'
import BlueprintPost from './views/BlueprintPost.vue'
import BlueprintEdit from './views/BlueprintEdit.vue'
import Login from './views/Login.vue'
import Register from './views/Register.vue'
import Validate from './views/Validate.vue'
import Contact from './views/Contact.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: BlueprintList,
      props: {default: true, listType: '', sort_field: 'CreatedAt'}
    },
    {
      path: '/mine',
      name: 'mine',
      component: BlueprintList,
      props: {default: true, listType: '/mine', sort_field: 'CreatedAt'}
    },
    {
      path: '/starred',
      name: 'starred',
      component: BlueprintList,
      props: {default: true, listType: '/starred', sort_field: 'CreatedAt'}
    },
    {
      path: '/popular',
      name: 'popular',
      component: BlueprintList,
      props: {default: true, listType: '', sort_field: 'StarCount'}
    },
    {
      path: '/post',
      name: 'blueprint-new',
      component: BlueprintPost
    },
    {
      path: '/edit/:id',
      name: 'blueprint-edit',
      component: BlueprintEdit,
      props: true
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      props: {default: true, reset: false}
    },
    {
      path: '/reset',
      name: 'reset',
      component: Register,
      props: {default: true, reset: true}
    },
    {
      path: '/register/:id',
      name: 'validate_invitation',
      component: Validate,
      props: (route) => ({
        invitationid: route.params.id,
        reset:  false
      })
    },
    {
      path: '/reset/:id',
      name: 'reset password',
      component: Validate,
      props: (route) => ({
        invitationid: route.params.id,
        reset:  true
      })
    },
    {
      path: '/contact',
      name: 'contact',
      component: Contact
    },
    {
      path: '/:id',
      name: 'blueprint',
      component: BlueprintView,
      props: true
    },
    {
      path: '*',
      component: BlueprintList
    }
  ]
})
