import Vue from 'vue';
import Vuex from 'vuex';
import VueMeta from 'vue-meta';
import VueClipboard from 'vue-clipboard2'
import App from './App.vue';

import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faPlusSquare,
	faTimes,
	faEdit,
	faStar,
	faClipboardList } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import router from './router';
import BootstrapVue from 'bootstrap-vue';
import moment from 'moment';
import store from './store';
import axios from 'axios';

import 'simplemde/dist/simplemde.min.css';

library.add(faPlusSquare,
	faTimes,
	faEdit,
	faStar,
	faClipboardList)

axios.defaults.baseURL = '/api';

Vue.use(Vuex);
Vue.use(VueMeta)
Vue.use(VueClipboard);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

Vue.filter('formatDate', function(parmval) {
  if (parmval) {
    return moment(String(parmval)).fromNow()
  }
});

new Vue({ router, store, render: createElement => createElement(App) }).$mount('#app');