import entitySpritesheetJSON from 'factorio-data/data/graphics/LREntitySpritesheet.json';
import iconSpritesheetJSON from 'factorio-data/data/graphics/iconSpritesheet.json';
import entitySpritesheetPNG from 'factorio-data/data/graphics/LREntitySpritesheet.png';
import iconSpritesheetPNG from 'factorio-data/data/graphics/iconSpritesheet.png';
import spriteDataBuilder from './spriteDataBuilder';
import FD from 'factorio-data';
import Blueprint from './blueprint';
import util from './util';
import _ from 'lodash';

export default {
  render,
  entitySpritesheetJSON,
  iconSpritesheetJSON,
  entitySpritesheetPNG,
  iconSpritesheetPNG,
  cssClipIcon,
  FD
}

function cssClipIcon(iconId) {
  if(!iconId) {
    return "display:none;"
  }
  let entityData = FD.items[util.convertNameToGameData(iconId)];
  if(entityData) {
    const iconPath = entityData.icon;
    const frameData = iconSpritesheetJSON.frames[iconPath];
    if (frameData) {
    const frame = frameData.frame;
    let result = "width: " + frame.w + "px;height:" 
      + frame.h +"px;background:url(" + iconSpritesheetPNG 
      + ");background-position:" + -frame.x + "px " + -frame.y + "px;";
    return result;
    }
  } else {
    return "display:none;"
  }
}

function render(ctx, entities) {
  return Promise.all([fetch(entitySpritesheetPNG), fetch(iconSpritesheetPNG)])
    .then((response) => {
      return Promise.all([response[0].blob(), response[1].blob()]);
    })
    .then((blobs) => {
      return Promise.all([createImageBitmap(blobs[0]), createImageBitmap(blobs[1])]);
    })
    .then((spriteSheets) => {
      let spriteSheet = spriteSheets[0];
      let icons = spriteSheets[1];
      render0(ctx, entities, spriteSheet, icons);
    })
}


function render0(ctx, entities, entitySpriteSheet, iconSpriteSheet) {
  const minXBp = _.minBy(entities, 'position.x').position.x - 2;
  const maxXBp = _.maxBy(entities, 'position.x').position.x + 2;
  const minYBp = _.minBy(entities, 'position.y').position.y - 2;
  const maxYBp = _.maxBy(entities, 'position.y').position.y + 2;
  const deltaX = maxXBp - minXBp;
  const deltaY = maxYBp - minYBp;
  const deltaMax = Math.max(deltaX, deltaY);
  const tileSize = 32;
  ctx.canvas.width = tileSize * deltaMax;
  ctx.canvas.height = tileSize * deltaMax;
  let scale = tileSize / 32;

  const convertedEntities = _.forEach(entities, (e) => {
    e.name = util.convertNameToGameData(e.name);
    if(e.recipe) {
      e.recipe = util.convertNameToGameData(e.recipe);
    }
  });

  let blueprint = new Blueprint({
    entities: _.filter(convertedEntities, (e) => {
      return FD.entities[e.name]
    }),
    version: 0,
    icons: [],
    item: 'blueprint'
  });

  let entitiesByType = _.groupBy(blueprint.entities.valuesArray(), 'name');
  for(let entityType in entitiesByType) {
    let entityData = FD.entities[entityType];
    let drawDataFn = spriteDataBuilder.generateGraphics(entityData);
    if(drawDataFn) {
      _.forEach(entitiesByType[entityType], (instance) => {
        let graph = drawDataFn({
          hr: false,
          bp: blueprint,
          dir: instance.direction,
          dirType: instance.directionType,
          position: instance.position,
          name: instance.name,
          generateConnector: false,
          assemblerPipeDirection: 'input',
          operator: '',
          assemblerCraftsWithFluid: false,
          chemicalPlantDontConnectOutput: false,
          trainStopColor: null
        });

        _.forEach(graph, (data) => {
          const spriteData = entitySpritesheetJSON.frames[data.filename];
          let frame = spriteData.frame;

          if(!data.shift) {
            data.shift = [0,0];
          }
          if (!data.x) {
              data.x = 0
          }
          if (!data.y) {
              data.y = 0
          }
          if (!data.divW) {
              data.divW = 1
          }
          if (!data.divH) {
              data.divH = 1
          }
          if (!data.scale) {
              data.scale = 1
          }

          if (data.scale) {
              scale *= data.scale;
          }
          let height = data.height;
          if (!data.squishY) {
              data.squishY = 1
          }
          if(!data.rotAngle) {
            data.rotAngle = 0;
          }

          let anchorX = data.anchorX === undefined ? 0.5 : data.anchorX
          let anchorY = data.anchorY === undefined ? 0.5 : data.anchorY
          ctx.setTransform(scale, 0, 0, scale, 
              tileSize * (instance.position.x - minXBp + data.shift[0]),
              tileSize * (instance.position.y - minYBp + data.shift[1]))

          let textureX = frame.x + data.x;
          let textureY = frame.y + data.y;
          let textureW = data.width / data.divW;
          let textureH = data.height / data.divH;
          // CACHE LOCAL BOUNDS
          let minX = textureW * -anchorX * data.scale
          let minY = textureH * -anchorY * data.scale / data.squishY
          let maxX = textureW * (1 - anchorX) * data.scale
          let maxY = textureH * (1 - anchorY) * data.scale / data.squishY

          if(data.rotAngle !== 0) {
            let rotation = Math.PI * data.rotAngle / 180;
            ctx.rotate(rotation);
          }
          ctx.drawImage(entitySpriteSheet, textureX, textureY, textureW, textureH, 
            minX, minY, maxX - minX, maxY - minY);
        })

        if(instance.recipe) {
          let recipeData = FD.items[util.convertNameToGameData(instance.recipe)];
          if(recipeData) {
            const iconPath = recipeData.icon;
            const frameData = iconSpritesheetJSON.frames[iconPath];
            if (frameData) {
              const iconFrame = frameData.frame;
              let iconScale = entityData.size.width / 3;
              ctx.setTransform(iconScale, 0, 0, iconScale, 
                tileSize * (instance.position.x - minXBp),
                tileSize * (instance.position.y - minYBp) - 10);

              let grd = ctx.createRadialGradient(0, 0, 10, 0, 0, iconFrame.w);
              grd.addColorStop(0, "#000000FF");
              grd.addColorStop(1, "#00000000");
              ctx.fillStyle = grd;
              ctx.beginPath();
              ctx.arc(0, 0, iconFrame.w, 0, 2 * Math.PI);
              ctx.fill();
              ctx.drawImage(iconSpriteSheet, iconFrame.x, iconFrame.y, iconFrame.w, iconFrame.h,
                            -iconFrame.w / 2, -iconFrame.h / 2, iconFrame.w, iconFrame.h);
            }
          }
        }
      });
    }
  }
}
